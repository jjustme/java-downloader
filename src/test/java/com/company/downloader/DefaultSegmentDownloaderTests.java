package com.company.downloader;

import com.company.downloader.exceptions.RetryLimitExceeded;
import com.company.downloader.utils.IOUtils;
import com.google.common.net.HttpHeaders;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.http.ContentTooLongException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicHeader;
import org.joda.time.Duration;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DefaultSegmentDownloaderTests {

    private Injector injector;

    @Mock
    private ValidatingHttpClient httpClient;

    @Mock
    private FileSystemFacade fileSystem;

    @Mock
    private RetryPolicy retryPolicy;

    @Mock
    private IOUtils ioUtils;

    @Mock
    private File segmentFile;

    private final String segmentPath = "c:\\temp\\output\\segment.skd";

    private final String dummyFileData = "dummy file data";

    // Happy path test.
    @Test
    public void testDownload1() throws Exception {
        Mockito.when(this.segmentFile.length()).thenReturn((long) this.dummyFileData.length());

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        List<Mirror> mirrors = new ArrayList<>();
        Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com");
        mirrors.add(mirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        SegmentDownloadResult downloadResult = downloader.call();
        org.junit.Assert.assertEquals(0, downloadResult.getSegmentIndex());
        org.junit.Assert.assertEquals(0, downloadResult.getRetryCount());
        org.junit.Assert.assertEquals(this.segmentPath, downloadResult.getSegmentPath());
        org.junit.Assert.assertEquals(this.dummyFileData.length(), downloadResult.getSegmentSize());
    }

    // Verify correct exception is thrown when more data than expected was downloaded.
    @Test(expected = ContentTooLongException.class)
    public void testDownload2() throws Exception {
        Mockito.when(this.segmentFile.length()).thenReturn(this.dummyFileData.length() + 1L);

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        List<Mirror> mirrors = new ArrayList<>();
        Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com");
        mirrors.add(mirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        downloader.call();
    }

    // Verify correct exception is thrown when retry limit is exceeded.
    @Test(expected = RetryLimitExceeded.class)
    public void testDownload3() throws Exception {
        Mockito.when(this.segmentFile.length()).thenReturn(this.dummyFileData.length() - 1L);

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        List<Mirror> mirrors = new ArrayList<>();
        Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com");
        mirrors.add(mirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        downloader.call();
    }

    // Verify retry works.
    @Test
    public void testDownload4() throws Exception {
        Mockito.when(this.segmentFile.length()).thenReturn(10L).thenReturn((long) this.dummyFileData.length());

        RetryResult retryResult = new RetryResult();
        retryResult.shouldRetry = true;
        retryResult.delay = Duration.ZERO;
        Mockito.when(this.retryPolicy.getRetry(0)).thenReturn(retryResult);
        retryResult = new RetryResult();
        retryResult.shouldRetry = false;
        Mockito.when(this.retryPolicy.getRetry(1)).thenReturn(retryResult);

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        List<Mirror> mirrors = new ArrayList<>();
        Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com");
        mirrors.add(mirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        downloader.call();
    }

    // Verify headers values are correctly added to the HTTP request
    @Test
    public void testDownload5() throws Exception {
        Mockito.when(this.segmentFile.length()).thenReturn((long) this.dummyFileData.length());

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        List<Mirror> mirrors = new ArrayList<>();
        final Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com");
        List<Header> headers = new ArrayList<>();
        String cookie = "key=value";
        headers.add(new BasicHeader("Cookie", cookie));

        mirror.headers = headers;
        mirrors.add(mirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        downloader.call();

        Mockito.verify(this.httpClient, Mockito.times(1)).execute(Mockito.argThat(new ArgumentMatcher<HttpGet>() {
            @Override
            public boolean matches(Object o) {
                HttpGet request = (HttpGet) o;
                for (Header header : mirror.headers) {
                    boolean found = false;

                    for (Header requestHeader : request.getAllHeaders()) {
                        if (requestHeader.getName().equalsIgnoreCase(header.getName()) && requestHeader.getValue().equalsIgnoreCase(header.getValue())) {
                            found = true;
                        }
                    }

                    if (!found) {
                        return false;
                    }
                }

                return true;
            }
        }), Mockito.anyLong(), Mockito.anyLong());
    }

    // Test download is resumed from the right byte on retry to new mirror.
    @Test
    public void testDownload6() throws Exception {
        RetryResult retryResult = new RetryResult();
        retryResult.shouldRetry = true;
        retryResult.delay = Duration.ZERO;
        Mockito.when(this.retryPolicy.getRetry(0)).thenReturn(retryResult);

        Mockito.when(this.segmentFile.length()).thenReturn(5L).thenReturn((long) this.dummyFileData.length());

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        final Mirror firstMirror = new Mirror();
        firstMirror.link = URI.create("http://www.google.com");

        final Mirror secondMirror = new Mirror();
        secondMirror.link = URI.create("http://secondmirror.com");

        List<Mirror> mirrors = new ArrayList<>();
        mirrors.add(firstMirror);
        mirrors.add(secondMirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        downloader.call();

        Mockito.verify(this.httpClient, Mockito.times(1)).execute(Mockito.argThat(new ArgumentMatcher<HttpGet>() {
            @Override
            public boolean matches(Object o) {
                HttpGet request = (HttpGet) o;
                return request.getURI().compareTo(URI.create("http://www.google.com")) == 0;
            }
        }), Mockito.anyLong(), Mockito.anyLong());

        Mockito.verify(this.httpClient, Mockito.times(1)).execute(Mockito.argThat(new ArgumentMatcher<HttpGet>() {
            @Override
            public boolean matches(Object o) {
                HttpGet request = (HttpGet)o;
                return request.getURI().compareTo(URI.create("http://secondmirror.com")) == 0;
            }
        }), Mockito.anyLong(), Mockito.anyLong());
    }

    // Verify data is downloaded correctly
    @Test
    public void testDownload7() throws Exception {
        final StringBuilder copiedData = new StringBuilder();
        Mockito.when(this.ioUtils.copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq((long) this.dummyFileData.length())))
                .then(new Answer<Long>() {
                    @Override
                    public Long answer(InvocationOnMock invocationOnMock) throws Throwable {
                        InputStream input = (InputStream) invocationOnMock.getArguments()[0];
                        copiedData.append(org.apache.commons.io.IOUtils.toString(input));
                        return (Long) invocationOnMock.getArguments()[3];
                    }
                });

        Mockito.when(this.segmentFile.length()).thenReturn((long) this.dummyFileData.length());

        FileOutputStream fileOutputStream = Mockito.mock(FileOutputStream.class);
        Mockito.when(this.fileSystem.getFileOutputStream(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(fileOutputStream);

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        final Mirror firstMirror = new Mirror();
        firstMirror.link = URI.create("http://www.google.com");

        List<Mirror> mirrors = new ArrayList<>();
        mirrors.add(firstMirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        downloader.call();

        Mockito.verify(this.ioUtils, Mockito.times(1)).copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq((long) this.dummyFileData.length()));
        org.junit.Assert.assertEquals(this.dummyFileData, copiedData.toString());
    }

    // Verify data is downloaded correctly on retry
    @Test
    public void testDownload8() throws Exception {
        Mockito.when(this.retryPolicy.getRetry(Mockito.anyInt())).then(invocationOnMock2 -> {
            RetryResult retryResult = new RetryResult();
            retryResult.shouldRetry = true;
            retryResult.delay = Duration.ZERO;
            return retryResult;
        }).then(invocationOnMock1 -> {
            RetryResult secondRetryResult = new RetryResult();
            secondRetryResult.shouldRetry = false;
            return secondRetryResult;
        });

        final StringBuilder copiedData = new StringBuilder();
        Mockito.when(this.ioUtils.copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq((long) this.dummyFileData.length())))
                .then(invocationOnMock -> {
                    InputStream input = (InputStream) invocationOnMock.getArguments()[0];
                    String inputString = org.apache.commons.io.IOUtils.toString(input).substring(0, 10);
                    copiedData.append(inputString);
                    return (long) inputString.length();
                });

        Mockito.when(this.ioUtils.copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq(this.dummyFileData.length() - 10L)))
                .then(invocationOnMock -> {
                    InputStream input = (InputStream) invocationOnMock.getArguments()[0];
                    String inputString = org.apache.commons.io.IOUtils.toString(input);
                    copiedData.append(inputString);
                    return (long) inputString.length();
                });

        Mockito.when(this.segmentFile.length()).thenReturn(10L).thenReturn((long) this.dummyFileData.length());

        FileOutputStream fileOutputStream = Mockito.mock(FileOutputStream.class);
        Mockito.when(this.fileSystem.getFileOutputStream(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(fileOutputStream);

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        final Mirror firstMirror = new Mirror();
        firstMirror.link = URI.create("http://www.google.com");

        List<Mirror> mirrors = new ArrayList<>();
        mirrors.add(firstMirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        downloader.call();

        Mockito.verify(this.ioUtils, Mockito.times(1)).copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq((long) this.dummyFileData.length()));
        Mockito.verify(this.ioUtils, Mockito.times(1)).copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq(this.dummyFileData.length() - 10L));
        org.junit.Assert.assertEquals(this.dummyFileData, copiedData.toString());
    }

    // Verify data is downloaded correctly on retry when exception is thrown
    @Test
    public void testDownload9() throws Exception {
        Mockito.when(this.retryPolicy.getRetry(Mockito.anyInt())).then(invocationOnMock2 -> {
            RetryResult retryResult = new RetryResult();
            retryResult.shouldRetry = true;
            retryResult.delay = Duration.ZERO;
            return retryResult;
        }).then(invocationOnMock1 -> {
            RetryResult secondRetryResult = new RetryResult();
            secondRetryResult.shouldRetry = false;
            return secondRetryResult;
        });

        final StringBuilder copiedData = new StringBuilder();
        Mockito.when(this.ioUtils.copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq((long) this.dummyFileData.length())))
                .then(invocationOnMock -> {
                    InputStream input = (InputStream) invocationOnMock.getArguments()[0];
                    String inputString = org.apache.commons.io.IOUtils.toString(input).substring(0, 10);
                    copiedData.append(inputString);
                    throw new IOException("Connection interruption");
                });

        Mockito.when(this.ioUtils.copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq(this.dummyFileData.length() - 10L)))
                .then(invocationOnMock -> {
                    InputStream input = (InputStream) invocationOnMock.getArguments()[0];
                    String inputString = org.apache.commons.io.IOUtils.toString(input);
                    copiedData.append(inputString);
                    return (long) inputString.length();
                });

        Mockito.when(this.segmentFile.length()).thenReturn(10L).thenReturn((long) this.dummyFileData.length());

        FileOutputStream fileOutputStream = Mockito.mock(FileOutputStream.class);
        Mockito.when(this.fileSystem.getFileOutputStream(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(fileOutputStream);

        DefaultSegmentDownloader downloader = injector.getInstance(DefaultSegmentDownloader.class);
        downloader.setRange(0, this.dummyFileData.length() - 1);

        final Mirror firstMirror = new Mirror();
        firstMirror.link = URI.create("http://www.google.com");

        List<Mirror> mirrors = new ArrayList<>();
        mirrors.add(firstMirror);

        downloader.setMirrors(mirrors);
        downloader.setOutputPath(this.segmentPath);
        downloader.call();

        Mockito.verify(this.ioUtils, Mockito.times(1)).copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq((long) this.dummyFileData.length()));
        Mockito.verify(this.ioUtils, Mockito.times(1)).copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.eq(0L), Mockito.eq(this.dummyFileData.length() - 10L));
        org.junit.Assert.assertEquals(this.dummyFileData, copiedData.toString());
    }

    @Before
    public void testInitialize() throws Exception {
        MockitoAnnotations.initMocks(this);
        Mockito.when(this.httpClient.execute(Mockito.any(HttpGet.class), Mockito.anyLong(), Mockito.anyLong())).thenAnswer(new Answer<HttpResponse>() {
            @Override
            public HttpResponse answer(InvocationOnMock invocationOnMock) throws Throwable {
                HttpGet request = (HttpGet) invocationOnMock.getArguments()[0];
                Header range = request.getFirstHeader(HttpHeaders.RANGE);

                String dummyData = dummyFileData;

                if (range != null) {
                    String rangeValue = range.getValue();
                    Pattern pattern = Pattern.compile("bytes=(\\d+)-(\\d+)");
                    Matcher matcher = pattern.matcher(rangeValue);
                    if (matcher.find()) {
                        int firstByteIndex = Integer.parseInt(matcher.group(1));
                        int lastByteIndex = Integer.parseInt(matcher.group(2));
                        dummyData = dummyData.substring(firstByteIndex, lastByteIndex + 1);
                    }
                }

                HttpResponse response = Mockito.mock(HttpResponse.class);
                HttpEntity entity = Mockito.mock(HttpEntity.class);

                Mockito.when(response.getEntity()).thenReturn(entity);
                Mockito.when(entity.getContent()).thenReturn(org.apache.commons.io.IOUtils.toInputStream(dummyData));
                return response;
            }
        });

        FileOutputStream fileOutputStream = Mockito.mock(FileOutputStream.class);
        Mockito.when(this.fileSystem.getFileOutputStream(Mockito.anyString(), Mockito.anyBoolean())).thenReturn(fileOutputStream);

        RetryResult retryResult = new RetryResult();
        retryResult.shouldRetry = false;
        Mockito.when(this.retryPolicy.getRetry(Mockito.eq(0))).thenReturn(retryResult);

        Mockito.when(this.fileSystem.getFile(this.segmentPath)).thenReturn(this.segmentFile);

        Mockito.when(this.ioUtils.copyLarge(Mockito.any(InputStream.class), Mockito.any(OutputStream.class), Mockito.anyLong(), Mockito.anyLong())).then(new Answer<Long>() {
            @Override
            public Long answer(InvocationOnMock invocationOnMock) throws Throwable {
                InputStream input = (InputStream) invocationOnMock.getArguments()[0];
                OutputStream output = (OutputStream) invocationOnMock.getArguments()[1];
                long offset = (long) invocationOnMock.getArguments()[2];
                long length = (long) invocationOnMock.getArguments()[3];
                return org.apache.commons.io.IOUtils.copyLarge(input, output, offset, length);
            }
        });

        this.injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ValidatingHttpClient.class).toInstance(httpClient);
                bind(FileSystemFacade.class).toInstance(fileSystem);
                bind(RetryPolicy.class).toInstance(retryPolicy);
                bind(IOUtils.class).toInstance(ioUtils);
            }
        });
    }
}
