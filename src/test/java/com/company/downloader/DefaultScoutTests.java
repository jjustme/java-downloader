package com.company.downloader;

import com.google.common.net.HttpHeaders;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHttpResponse;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.IOException;
import java.net.URI;

public class DefaultScoutTests {
    private Injector injector;

    @Mock
    private CustomHttpClient httpClient;

    @Test
    public void testScout1() throws IOException {
        Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com");

        Mockito.when(this.httpClient.execute(Mockito.any(HttpGet.class))).then(invocationOnMock -> {
            BasicHttpEntity entity = new BasicHttpEntity();
            entity.setContentLength(20L);

            BasicHttpResponse response = new BasicHttpResponse(new ProtocolVersion("HTTP", 1, 1), 200, "OK");
            response.setEntity(entity);

            return response;
        });

        DefaultScout scout = injector.getInstance(DefaultScout.class);
        ScoutResult result = scout.scout(mirror);
        Assert.assertEquals(20L, result.fileSize);
        Assert.assertFalse(result.acceptsRanges);
        Assert.assertNull(result.filename);
    }

    @Test
    public void testScout2() throws IOException {
        Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com");

        Mockito.when(this.httpClient.execute(Mockito.any(HttpGet.class))).then(invocationOnMock -> {
            BasicHttpResponse response = new BasicHttpResponse(new ProtocolVersion("HTTP", 1, 1), 200, "OK");
            BasicHttpEntity entity = new BasicHttpEntity();
            entity.setContentLength(0L);

            response.setEntity(entity);
            response.addHeader(HttpHeaders.CONTENT_RANGE, "1-2/*");

            return response;
        });

        DefaultScout scout = injector.getInstance(DefaultScout.class);
        ScoutResult result = scout.scout(mirror);
        Assert.assertEquals(0L, result.fileSize);
        Assert.assertTrue(result.acceptsRanges);
        Assert.assertNull(result.filename);
    }

    @Test
    public void testScout3() throws IOException {
        Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com");

        Mockito.when(this.httpClient.execute(Mockito.any(HttpGet.class))).then(invocationOnMock -> {
            BasicHttpResponse response = new BasicHttpResponse(new ProtocolVersion("HTTP", 1, 1), 200, "OK");
            BasicHttpEntity entity = new BasicHttpEntity();
            entity.setContentLength(0L);

            response.setEntity(entity);
            response.addHeader(HttpHeaders.CONTENT_RANGE, "1-2/*");
            response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=importantfile.dat");

            return response;
        });

        DefaultScout scout = injector.getInstance(DefaultScout.class);
        ScoutResult result = scout.scout(mirror);
        Assert.assertEquals(0L, result.fileSize);
        Assert.assertTrue(result.acceptsRanges);
        Assert.assertEquals("importantfile.dat", result.filename);
    }

    @Test
    public void testScout4() throws IOException {
        Mirror mirror = new Mirror();
        mirror.link = URI.create("http://www.google.com/path/to/file/importantfile.dat");

        Mockito.when(this.httpClient.execute(Mockito.any(HttpGet.class))).then(invocationOnMock -> {
            BasicHttpResponse response = new BasicHttpResponse(new ProtocolVersion("HTTP", 1, 1), 200, "OK");
            BasicHttpEntity entity = new BasicHttpEntity();
            entity.setContentLength(0L);

            response.setEntity(entity);
            response.addHeader(HttpHeaders.CONTENT_RANGE, "1-2/*");

            return response;
        });

        DefaultScout scout = injector.getInstance(DefaultScout.class);
        ScoutResult result = scout.scout(mirror);
        Assert.assertEquals(0L, result.fileSize);
        Assert.assertTrue(result.acceptsRanges);
        Assert.assertEquals("importantfile.dat", result.filename);
    }

    @Before
    public void testInitialize() {
        MockitoAnnotations.initMocks(this);
        this.injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(CustomHttpClient.class).toInstance(httpClient);
            }
        });
    }
}
