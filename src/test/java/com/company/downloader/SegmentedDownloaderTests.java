package com.company.downloader;

import com.company.downloader.utils.DefaultIOUtils;
import com.company.downloader.utils.IOUtils;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.TypeLiteral;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SegmentedDownloaderTests {

    private Injector injector;

    @Mock
    private Scout scout;

    @Mock
    private RangeCalculator rangeCalculator;

    @Mock
    private SegmentDownloaderFactory segmentDownloaderFactory;

    @Mock
    private FileSystemFacade fileSystem;

    @Mock
    private DefaultExecutorCompletionWrapper executionService;

    @Test
    public void test1() throws Exception {
        Mockito.when(this.rangeCalculator.getRanges(Mockito.anyLong(), Mockito.anyInt())).then(invocationOnMock -> {
            List<ImmutablePair<Long, Long>> ranges = new ArrayList<>();

            ranges.add(new ImmutablePair<>(0L, 5L));
            ranges.add(new ImmutablePair<>(6L, 10L));

            return ranges;
        });

        List<Mirror> mirrors = new ArrayList<>();
        mirrors.add(new Mirror());

        DownloadConfiguration downloadConfiguration = new DownloadConfiguration();
        downloadConfiguration.maximumSimultaneousConnections = 2;
        downloadConfiguration.mirrors = mirrors;

        SegmentedDownloader downloader = this.injector.getInstance(SegmentedDownloader.class);
        downloader.initialize(downloadConfiguration);
    }

    @Before
    public void testInitialize() throws IOException {
        MockitoAnnotations.initMocks(this);

        File file = Mockito.mock(File.class);
        Mockito.when(file.isFile()).thenReturn(true);
        Mockito.when(this.fileSystem.getFile(Mockito.anyString())).thenReturn(file);
        Mockito.when(this.fileSystem.createTempFile(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(file);

        Mockito.when(this.scout.scout(Mockito.any(Mirror.class))).then(invocationOnMock -> {
            ScoutResult result = new ScoutResult();
            result.filename = "filename.bin";
            result.acceptsRanges = true;
            result.fileSize = 20L;

            return result;
        });

        this.injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(RangeCalculator.class).toInstance(rangeCalculator);
                bind(Scout.class).toInstance(scout);
                bind(SegmentDownloaderFactory.class).toInstance(segmentDownloaderFactory);
                bind(FileSystemFacade.class).toInstance(fileSystem);
                bind(IOUtils.class).to(DefaultIOUtils.class);
                bind(new TypeLiteral<ExecutorCompletionWrapper<SegmentDownloader, SegmentDownloadResult>>() {}).toInstance(executionService);
            }
        });
    }
}
