package com.company.downloader;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class DefaultRangeCalculatorTests {

    @Test
    public void testRanges1() {
        DefaultRangeCalculator rangeCalculator = new DefaultRangeCalculator();
        List<ImmutablePair<Long, Long>> ranges = rangeCalculator.getRanges(100, 1);

        Assert.assertEquals(1, ranges.size());

        ImmutablePair<Long, Long> expectedRange = new ImmutablePair<>(0L, 99L);
        Assert.assertEquals(expectedRange, ranges.get(0));
    }

    @Test
    public void testRanges2() {
        DefaultRangeCalculator rangeCalculator = new DefaultRangeCalculator();
        List<ImmutablePair<Long, Long>> ranges = rangeCalculator.getRanges(100, 2);

        Assert.assertEquals(2, ranges.size());

        ImmutablePair<Long, Long> expectedRange = new ImmutablePair<>(0L, 49L);
        Assert.assertEquals(expectedRange, ranges.get(0));

        expectedRange = new ImmutablePair<>(50L, 99L);
        Assert.assertEquals(expectedRange, ranges.get(1));
    }

    @Test
    public void testRanges3() {
        DefaultRangeCalculator rangeCalculator = new DefaultRangeCalculator();
        List<ImmutablePair<Long, Long>> ranges = rangeCalculator.getRanges(97, 2);

        Assert.assertEquals(2, ranges.size());

        ImmutablePair<Long, Long> expectedRange = new ImmutablePair<>(0L, 47L);
        Assert.assertEquals(expectedRange, ranges.get(0));

        expectedRange = new ImmutablePair<>(48L, 96L);
        Assert.assertEquals(expectedRange, ranges.get(1));
    }

    @Test
    public void testRanges4() {
        DefaultRangeCalculator rangeCalculator = new DefaultRangeCalculator();
        List<ImmutablePair<Long, Long>> ranges = rangeCalculator.getRanges(97, 3);

        Assert.assertEquals(3, ranges.size());

        ImmutablePair<Long, Long> expectedRange = new ImmutablePair<Long, Long>(0L, 31L);
        Assert.assertEquals(expectedRange, ranges.get(0));

        expectedRange = new ImmutablePair<>(32L, 63L);
        Assert.assertEquals(expectedRange, ranges.get(1));

        expectedRange = new ImmutablePair<>(64L, 96L);
        Assert.assertEquals(expectedRange, ranges.get(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRangesZeroChunks() {
        DefaultRangeCalculator rangeCalculator = new DefaultRangeCalculator();
        rangeCalculator.getRanges(97, 0);
    }
}
