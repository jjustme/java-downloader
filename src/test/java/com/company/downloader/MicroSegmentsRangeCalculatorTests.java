package com.company.downloader;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class MicroSegmentsRangeCalculatorTests {
    private Injector injector;

    // Call getRanges without crashing
    @Test
    public void test1() {
        RangeCalculator rangeCalculator = this.injector.getInstance(RangeCalculator.class);
        rangeCalculator.getRanges(1024 * 1024, 0);
    }

    @Test
    public void test2() {
        RangeCalculator rangeCalculator = this.injector.getInstance(RangeCalculator.class);
        List<ImmutablePair<Long, Long>> ranges = rangeCalculator.getRanges(1024 * 1024, 0);

        Assert.assertEquals(1, ranges.size());
        ImmutablePair<Long, Long> range = ranges.get(0);
        Assert.assertEquals(0, (long) range.left);
        Assert.assertEquals((1024 * 1024) - 1, (long) range.right);
    }

    @Test(expected = IllegalArgumentException.class)
    public void test3() {
        RangeCalculator rangeCalculator = this.injector.getInstance(RangeCalculator.class);
        rangeCalculator.getRanges(-1, 0);
    }

    @Test
    public void test4() {
        RangeCalculator rangeCalculator = this.injector.getInstance(RangeCalculator.class);
        List<ImmutablePair<Long, Long>> ranges = rangeCalculator.getRanges(3 * 1024 * 1024, 0);

        Assert.assertEquals(2, ranges.size());
        ImmutablePair<Long, Long> range = ranges.get(0);
        Assert.assertEquals(0, (long) range.left);
        Assert.assertEquals((2 * 1024 * 1024) - 1, (long) range.right);
        range = ranges.get(1);
        Assert.assertEquals(2 * 1024 * 1024, (long) range.left);
        Assert.assertEquals((3 * 1024 * 1024) - 1, (long) range.right);
    }

    @Before
    public void testInitialize() {
        this.injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(RangeCalculator.class).to(MicroSegmentsRangeCalculator.class);
                bind(int.class).annotatedWith(Names.named("MicroSegmentSize")).toInstance(2 * 1024 * 1024);
            }
        });
    }
}
