package com.company.downloader;

import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.ArrayList;
import java.util.List;

public class DefaultRangeCalculator implements RangeCalculator {
    public List<ImmutablePair<Long, Long>> getRanges(long fileSize, int chunks) {
        if (fileSize < 0) {
            throw new IllegalArgumentException("fileSize");
        }

        if (chunks < 1) {
            throw new IllegalArgumentException("chunks");
        }

        List<ImmutablePair<Long, Long>> ranges = new ArrayList<>(chunks);
        long chunkSize = (long)Math.floor(fileSize / (double)chunks);
        long chunkOffset = 0;

        for (int chunkIndex = 0; chunkIndex < chunks; chunkIndex++) {
            ImmutablePair<Long, Long> range;
            if (chunkIndex == chunks - 1) {
                // assign rest of the bytes the last chunk
                range = new ImmutablePair<>(chunkOffset, fileSize - 1);
            } else {
                range = new ImmutablePair<>(chunkOffset, chunkOffset + chunkSize - 1);
            }

            ranges.add(range);
            chunkOffset += chunkSize;
        }

        return ranges;
    }
}
