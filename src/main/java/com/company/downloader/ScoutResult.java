package com.company.downloader;

import javax.annotation.Nullable;

public class ScoutResult {
    public long fileSize;
    @Nullable
    public String filename;
    public boolean acceptsRanges;
}
