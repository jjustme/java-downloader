package com.company.downloader.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DefaultIOUtils implements IOUtils {
    @Override
    public long copyLarge(InputStream input, OutputStream output, long inputOffset, long length) throws IOException {
        return org.apache.commons.io.IOUtils.copyLarge(input, output, inputOffset, length);
    }
}
