package com.company.downloader.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface IOUtils {
    long copyLarge(InputStream input, OutputStream output, long offset, long bytesToCopy) throws IOException;
}
