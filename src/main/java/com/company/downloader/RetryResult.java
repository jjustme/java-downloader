package com.company.downloader;

import org.joda.time.Duration;

public class RetryResult {
    public boolean shouldRetry;
    public Duration delay;
}
