package com.company.downloader;

import org.apache.commons.lang3.tuple.ImmutablePair;

public class SegmentContext {
    public int segmentIndex;
    public String outputPath;
    public ImmutablePair<Long, Long> range;
}
