package com.company.downloader;

import com.company.downloader.exceptions.HeaderValueException;
import com.company.downloader.exceptions.RetryLimitExceeded;
import com.company.downloader.exceptions.TooManyHeadersException;
import com.company.downloader.utils.IOUtils;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.commons.io.input.CountingInputStream;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.http.ContentTooLongException;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DefaultSegmentDownloader implements SegmentDownloader {
    private int segmentIndex;

    private String outputPath;

    private ImmutablePair<Long, Long> range;

    private List<Mirror> mirrors;

    private int retryCount = 0;

    private long downloadedBytes = 0;

    private CountingInputStream inputStream;

    @Inject
    private Provider<ValidatingHttpClient> httpClientProvider;

    @Inject
    private FileSystemFacade fileSystem;

    @Inject
    private RetryPolicy retryPolicy;

    @Inject
    private Logger logger;

    @Inject
    private IOUtils ioUtils;

    public int getIndex() {
        return this.segmentIndex;
    }

    @Override
    public long getDownloadedBytes() {
        if (this.inputStream != null) {
            return this.downloadedBytes + this.inputStream.getByteCount();
        } else {
            return this.downloadedBytes;
        }
    }

    public void setIndex(int segmentIndex) {
        this.segmentIndex = segmentIndex;
    }

    public void setRange(long from, long to) {
        this.range = new ImmutablePair<>(from, to);
    }

    public void setMirrors(List<Mirror> mirrors) {
        this.mirrors = mirrors;
    }

    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }

    public SegmentDownloadResult call() throws Exception {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        long segmentSize = this.range.right - this.range.left + 1;

        boolean retry = true;
        while(retry) {
            // choose mirror to use
            Mirror mirror = this.mirrors.get((this.segmentIndex + retryCount) % this.mirrors.size());

            // calculate start and end range
            long startByteIndex = this.range.left + this.downloadedBytes;
            long lastByteIndex = this.range.right;
            long bytesLeftToDownload = lastByteIndex - startByteIndex + 1;

            this.logger.info(
                    String.format(
                            "Segment %d starting attempt %d with range %d-%d using mirror %s",
                            this.segmentIndex,
                            this.retryCount,
                            startByteIndex,
                            lastByteIndex,
                            mirror.link)
            );

            HttpGet request = new HttpGet(mirror.link);
            request.addHeader(HttpHeaders.RANGE, String.format("bytes=%d-%d", startByteIndex, lastByteIndex));
            if (mirror.headers != null) {
                for (Header header : mirror.headers) {
                    request.addHeader(header);
                }
            }

            HttpResponse response = null;
            try (FileOutputStream file = this.fileSystem.getFileOutputStream(this.outputPath, true);
                 ValidatingHttpClient httpClient = this.httpClientProvider.get()) {
                response = httpClient.execute(request, startByteIndex, lastByteIndex);
                this.inputStream = new CountingInputStream(response.getEntity().getContent());
                this.ioUtils.copyLarge(this.inputStream, file, 0L, bytesLeftToDownload);
            }
            catch (IOException | TooManyHeadersException | HeaderValueException e) {
                String logMessage = String.format("Segment %d encountered transient exception.", this.segmentIndex);
                this.logger.log(Level.WARNING, logMessage, e);
            } finally {
                if (this.inputStream != null) {
                    this.inputStream.close();
                }

                request.releaseConnection();
            }

            this.downloadedBytes = this.fileSystem.getFile(this.outputPath).length();

            if (this.downloadedBytes == segmentSize){
                retry = false;

                this.logger.info(
                        String.format(
                                "Segment %d completed downloading the expected number of bytes: %d after %d retries.",
                                this.segmentIndex,
                                segmentSize,
                                this.retryCount)
                );
            }
            else {
                if (this.downloadedBytes < segmentSize) {
                    this.logger.warning(
                            String.format(
                                    "Segment %d downloaded %d bytes, expected to download %d, %d bytes remain to be downloaded",
                                    this.segmentIndex,
                                    this.downloadedBytes,
                                    segmentSize,
                                    segmentSize - this.downloadedBytes));

                    RetryResult retryResult = this.retryPolicy.getRetry(retryCount++);
                    retry = retryResult.shouldRetry;

                    if (retry) {
                        this.logger.info(
                                String.format(
                                        "Segment %d starting retry attempt %d in %d seconds.",
                                        this.segmentIndex,
                                        this.retryCount,
                                        retryResult.delay.getStandardSeconds())
                        );
                    } else {
                        this.logger.warning(
                                String.format(
                                        "Segment %d not retrying after %d retries.",
                                        this.segmentIndex,
                                        this.retryCount)
                        );

                        throw new RetryLimitExceeded();
                    }

                    Thread.sleep(retryResult.delay.getMillis());
                } else {
                    this.logger.severe(
                            String.format(
                                    "Segment %d downloaded %d bytes, expected to only download %d, an extra %d bytes were downloaded, not retrying.",
                                    this.segmentIndex,
                                    this.downloadedBytes,
                                    segmentSize,
                                    this.downloadedBytes - segmentSize
                            )
                    );

                    throw new ContentTooLongException("More bytes than the expected amount were downloaded.");
                }
            }
        }

        stopWatch.stop();
        SegmentDownloadResult downloadResult = new SegmentDownloadResult();
        downloadResult.setSegmentIndex(this.segmentIndex);
        downloadResult.setSegmentPath(this.outputPath);
        downloadResult.setSegmentSize(segmentSize);
        downloadResult.setRetryCount(this.retryCount);
        downloadResult.setElapsedTime(stopWatch.getTime());

        return downloadResult;
    }
}
