package com.company.downloader;

import java.util.List;

public class DownloadConfiguration {
    public List<Mirror> mirrors;
    public int maximumSimultaneousConnections;
    public String temporaryDirectory;
    public String outputPath;
}
