package com.company.downloader;

public interface SegmentDownloaderFactory {
    SegmentDownloader createSegmentDownloader();
}
