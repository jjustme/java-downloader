package com.company.downloader;

import com.google.common.net.HttpHeaders;
import com.google.inject.Inject;
import com.google.inject.Provider;
import org.apache.http.*;
import org.apache.http.client.methods.HttpGet;

import java.io.IOException;
import java.net.URI;

public class DefaultScout implements Scout {
    @Inject
    private Provider<CustomHttpClient> httpClientProvider;

    @Override
    public ScoutResult scout(Mirror mirror) throws IOException {
        HttpGet request = new HttpGet(mirror.link);
        if (mirror.headers != null) {
            for (Header header : mirror.headers) {
                request.addHeader(header);
            }
        }

        request.addHeader(HttpHeaders.RANGE, "bytes=0-");

        ScoutResult result = new ScoutResult();
        try(CustomHttpClient httpClient = this.httpClientProvider.get()) {
            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();

            result.fileSize = entity.getContentLength();
            result.filename = getFilename(request, response);
            result.acceptsRanges = response.containsHeader(HttpHeaders.CONTENT_RANGE);

            return result;
        }
    }

    private String getFilename(HttpRequest request, HttpResponse response) {
        String filename = this.parseContentDisposition(response);
        if (filename == null || filename.isEmpty()) {
            filename = this.getFilenameFromPath(request);
        }

        if (filename.isEmpty()) {
            filename = null;
        }

        return filename;
    }

    private String getFilenameFromPath(HttpRequest request) {
        String path = URI.create(request.getRequestLine().getUri()).getPath();
        return path.substring(path.lastIndexOf("/") + 1);
    }

    private String parseContentDisposition(HttpResponse response) {
        if (response.containsHeader(HttpHeaders.CONTENT_DISPOSITION)) {
            Header contentDisposition = response.getFirstHeader(HttpHeaders.CONTENT_DISPOSITION);
            HeaderElement[] headerElements = contentDisposition.getElements();
            if (headerElements.length > 0) {
                HeaderElement headerElement = headerElements[0];
                if (headerElement.getName().equalsIgnoreCase("attachment")) {
                    NameValuePair pair = headerElement.getParameterByName("filename");
                    if (pair != null) {
                        return pair.getValue();
                    }
                }
            }
        }
        return null;
    }
}
