package com.company.downloader.exceptions;

public class TooManyHeadersException extends Exception {
    public TooManyHeadersException(String format) {
        super(format);
    }
}
