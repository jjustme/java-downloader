package com.company.downloader.exceptions;

public class HeaderValueException extends Exception {
    public HeaderValueException(String format) {
        super(format);
    }
}
