package com.company.downloader;

import com.company.downloader.utils.IOUtils;
import com.google.inject.Inject;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SegmentedDownloader implements Downloader {
    @Inject
    private RangeCalculator rangeCalculator;

    @Inject
    private SegmentDownloaderFactory segmentDownloaderFactory;

    @Inject
    private Scout scout;

    @Inject
    private Logger logger;

    @Inject
    private FileSystemFacade fileSystem;

    @Inject
    private IOUtils ioUtils;

    @Inject
    private ExecutorCompletionWrapper<SegmentDownloader, SegmentDownloadResult> executionService;

    private List<Mirror> mirrors;

    private int numberOfChunks;

    private String temporaryDirectory;

    private String outputPath;

    public void initialize(DownloadConfiguration configuration) {
        this.mirrors = configuration.mirrors;
        this.numberOfChunks = configuration.maximumSimultaneousConnections;
        this.temporaryDirectory = configuration.temporaryDirectory;
        this.outputPath = configuration.outputPath;
    }

    @Override
    public void download() throws Exception {
        Set<SegmentContext> segmentContexts = this.getSegmentContexts();

        // get thread pool
        this.executionService.initialise(this.numberOfChunks);

        for (SegmentContext segmentStartInfo : segmentContexts) {
            SegmentDownloader segmentDownloader = this.segmentDownloaderFactory.createSegmentDownloader();
            segmentDownloader.setIndex(segmentStartInfo.segmentIndex);
            segmentDownloader.setOutputPath(segmentStartInfo.outputPath);
            segmentDownloader.setRange(segmentStartInfo.range.left, segmentStartInfo.range.right);
            segmentDownloader.setMirrors(new ArrayList<>(this.mirrors));

            this.executionService.submit(segmentDownloader);
        }

        Set<SegmentDownloadResult> downloadResults = new HashSet<>();
        int failedSegments = 0;
        for (int segmentIndex = 0; segmentIndex < segmentContexts.size(); segmentIndex++) {
            try {
                SegmentDownloadResult downloadResult = this.executionService.get();
                downloadResults.add(downloadResult);

                long secondsElapsed = downloadResult.getElapsedTime() / 1000;
                long kiloBytesDownloaded = downloadResult.getSegmentSize() / 1024;
                if (secondsElapsed > 0) {
                    this.logger.info(String.format("Segment %d completed in %d seconds at %d kB/s",
                            downloadResult.getSegmentIndex(), secondsElapsed, kiloBytesDownloaded / secondsElapsed));
                }
            } catch (Exception e) {
                failedSegments++;
                this.logger.severe(e.getMessage());
            }
        }

        this.executionService.shutdown();

        if (failedSegments > 0) {
            throw new Exception(String.format("%d out of %d segments threw an exception.", failedSegments, segmentContexts.size()));
        }

        this.joinSegments(downloadResults);
    }

    private void joinSegments(Set<SegmentDownloadResult> downloadResults) throws Exception {
        File outputFile = this.fileSystem.getFile(this.outputPath);
        if (!outputFile.createNewFile()) {
            throw new Exception(String.format("Could not create file: %s.", this.outputPath));
        }

        try (FileOutputStream fileOutputStream = this.fileSystem.getFileOutputStream(this.outputPath, false)) {
            for (SegmentDownloadResult downloadResult : downloadResults.stream().sorted(Comparator.comparingInt(SegmentDownloadResult::getSegmentIndex)).collect(Collectors.toList())) {
                this.logger.info(String.format("Copying segment %d from %s to output file.", downloadResult.getSegmentIndex(), downloadResult.getSegmentPath()));

                FileInputStream segmentStream = this.fileSystem.getFileInputStream(downloadResult.getSegmentPath());
                this.ioUtils.copyLarge(segmentStream, fileOutputStream, 0L, downloadResult.getSegmentSize());

                this.logger.info(String.format("Finished copying segment %d to output file.", downloadResult.getSegmentIndex()));
            }
        } catch (IOException e) {
            this.logger.severe(e.toString());
            throw e;
        }
    }

    private Set<SegmentContext> getSegmentContexts() throws Exception {
        ScoutResult scoutResult = this.scout.scout(this.mirrors.get(0));

        if (!scoutResult.acceptsRanges) {
            throw new Exception(String.format("Mirror: %s does not support range headers.", this.mirrors.get(0).link));
        }

        this.outputPath = this.getOutputPath(scoutResult.filename);
        long fileSize = scoutResult.fileSize;

        // get start info
        List<ImmutablePair<Long, Long>> ranges = this.rangeCalculator.getRanges(fileSize, this.numberOfChunks);
        Set<SegmentContext> segmentStartInfos = new HashSet<>(ranges.size());

        for (int segmentIndex = 0; segmentIndex < ranges.size(); segmentIndex++) {
            SegmentContext startInfo = new SegmentContext();
            startInfo.segmentIndex = segmentIndex;
            startInfo.outputPath = this.fileSystem.createTempFile(String.format("seg-%d-", segmentIndex), "dat", this.temporaryDirectory).getCanonicalPath();
            startInfo.range = ranges.get(segmentIndex);

            segmentStartInfos.add(startInfo);
        }

        return segmentStartInfos;
    }

    private String getOutputPath(String filename) throws Exception {
        File file = this.fileSystem.getFile(this.outputPath);

        if (file.isFile()) {
            return this.outputPath;
        } else if(file.isDirectory()) {
            if (filename != null && !filename.isEmpty()) {
                return new File(this.outputPath, filename).getPath();
            }
        }

        throw new Exception("Cannot determine output path, try specifying one.");
    }
}
