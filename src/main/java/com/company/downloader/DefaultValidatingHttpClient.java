package com.company.downloader;

import com.company.downloader.exceptions.HeaderValueException;
import com.company.downloader.exceptions.TooManyHeadersException;
import com.google.inject.Inject;
import org.apache.http.ContentTooLongException;
import org.apache.http.Header;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;
import java.util.Arrays;

public class DefaultValidatingHttpClient implements ValidatingHttpClient {
    @Inject
    private CloseableHttpClient httpClient;

    @Override
    public HttpResponse execute(HttpGet request, long startByteIndex, long lastByteIndex) throws Exception {
        HttpResponse response = this.httpClient.execute(request);
        this.validateResponse(request, response, startByteIndex, lastByteIndex);
        return response;
    }

    public void close() throws IOException {
        this.httpClient.close();
    }

    private void validateResponse(HttpGet request, HttpResponse response, long startByteIndex, long lastByteIndex) throws Exception {
        long bytesLeftToDownload = lastByteIndex - startByteIndex + 1;
        if (response.getEntity().getContentLength() != bytesLeftToDownload) {
            throw new ContentTooLongException(
                    String.format(
                            "Expected a content length of %d for request %s, instead got content length of %d from response %s.",
                            bytesLeftToDownload,
                            request,
                            response.getEntity().getContentLength(),
                            response));
        }

        Header[] rangeHeaders = response.getHeaders(HttpHeaders.CONTENT_RANGE);
        if (rangeHeaders.length != 1) {
            throw new TooManyHeadersException(
                    String.format(
                            "Expected a single Content-Range header, instead got multiple: %s",
                            Arrays.toString(rangeHeaders)
                    )
            );
        }

        String expectedRangeHeaderPrefix = String.format("bytes %d-%d", startByteIndex, lastByteIndex);
        if (!rangeHeaders[0].getValue().toLowerCase().startsWith(expectedRangeHeaderPrefix)) {
            throw new HeaderValueException(
                    String.format(
                            "Expected the Content-Range header to match the request %s, instead got: %s",
                            expectedRangeHeaderPrefix,
                            rangeHeaders[0].getValue())
            );
        }
    }
}
