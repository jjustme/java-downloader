package com.company.downloader;

import java.io.IOException;

public interface Scout {
    ScoutResult scout(Mirror mirror) throws IOException;
}
