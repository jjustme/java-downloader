package com.company.downloader;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class DefaultSegmentDownloaderFactory implements SegmentDownloaderFactory {
    @Inject
    private Provider<SegmentDownloader> segmentDownloaderProvider;

    public SegmentDownloader createSegmentDownloader() {
        return this.segmentDownloaderProvider.get();
    }
}
