package com.company.downloader;

import org.apache.http.Header;

import javax.annotation.Nullable;
import java.net.URI;
import java.util.List;

public class Mirror {
    public URI link;
    @Nullable
    public List<Header> headers;
}
