package com.company.downloader;

import java.util.List;
import java.util.concurrent.Callable;

public interface SegmentDownloader extends Callable<SegmentDownloadResult> {
    int getIndex();
    long getDownloadedBytes();
    void setIndex(int segmentIndex);
    void setRange(long from, long to);
    void setMirrors(List<Mirror> mirrors);
    void setOutputPath(String outputPath);
}
