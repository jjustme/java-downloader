package com.company.downloader;

public interface Downloader {
    void initialize(DownloadConfiguration configuration);
    void download() throws Exception;
}
