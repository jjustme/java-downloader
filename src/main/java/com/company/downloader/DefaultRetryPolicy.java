package com.company.downloader;

import org.joda.time.Duration;

public class DefaultRetryPolicy implements RetryPolicy {
    public RetryResult getRetry(int retryAttempts) {
        RetryResult result = new RetryResult();

        if (retryAttempts > 10) {
            result.shouldRetry = false;
            result.delay = null;
        } else {
            result.shouldRetry = true;
            result.delay = Duration.standardSeconds(5 + (5 * retryAttempts));
        }

        return result;
    }
}
