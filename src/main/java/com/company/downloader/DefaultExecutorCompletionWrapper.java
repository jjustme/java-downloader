package com.company.downloader;

import java.util.concurrent.*;

public class DefaultExecutorCompletionWrapper<TInput extends Callable<TOutput>, TOutput> implements ExecutorCompletionWrapper<TInput, TOutput> {
    private ExecutorService executorService;

    private ExecutorCompletionService<TOutput> completionService;

    private boolean isInitialised = false;

    @Override
    public void initialise(int numberOfThreads) {
        if (this.isInitialised) {
            throw new ExceptionInInitializerError();
        } else {
            this.isInitialised = true;
            this.executorService = Executors.newFixedThreadPool(numberOfThreads);
            this.completionService = new ExecutorCompletionService<>(this.executorService);
        }
    }

    @Override
    public void submit(TInput callable) {
        this.completionService.submit(callable);
    }

    @Override
    public TOutput get() throws InterruptedException, ExecutionException {
        return completionService.take().get();
    }

    @Override
    public void shutdown() {
        this.executorService.shutdown();
    }
}
