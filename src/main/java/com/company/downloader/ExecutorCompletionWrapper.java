package com.company.downloader;

import java.util.concurrent.ExecutionException;

public interface ExecutorCompletionWrapper<TInput, TOutput> {
    void initialise(int numberOfThreads);
    void submit(TInput callable);
    TOutput get() throws InterruptedException, ExecutionException;
    void shutdown();
}
