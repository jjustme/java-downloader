package com.company.downloader;

import java.io.*;

public interface FileSystemFacade {
    FileInputStream getFileInputStream(String path) throws FileNotFoundException;
    FileOutputStream getFileOutputStream(String path, boolean append) throws FileNotFoundException;
    File createTempFile(String prefix, String suffix, String directory) throws IOException;
    File getFile(String path);
}
