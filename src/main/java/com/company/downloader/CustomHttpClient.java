package com.company.downloader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import java.io.Closeable;
import java.io.IOException;

public interface CustomHttpClient extends Closeable {
    HttpResponse execute(HttpGet request) throws IOException;
}
