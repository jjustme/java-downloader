package com.company.downloader;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.ArrayList;
import java.util.List;

public class MicroSegmentsRangeCalculator implements RangeCalculator {
    @Inject @Named("MicroSegmentSize")
    private int segmentSize;

    @Override
    public List<ImmutablePair<Long, Long>> getRanges(long fileSize, int chunks) {
        if (fileSize < 0) {
            throw new IllegalArgumentException("fileSize");
        }

        List<ImmutablePair<Long, Long>> ranges = new ArrayList<>(chunks);

        long chunkOffset = 0;

        long bytesLeftToAllocate = fileSize;
        while (bytesLeftToAllocate > 0) {
            long startByteIndex = chunkOffset;
            long endByteIndex;

            if (bytesLeftToAllocate >= this.segmentSize) {
                endByteIndex = chunkOffset + this.segmentSize - 1;
            } else {
                endByteIndex = chunkOffset + bytesLeftToAllocate - 1;
            }

            ranges.add(new ImmutablePair<>(startByteIndex, endByteIndex));

            chunkOffset = endByteIndex + 1;
            bytesLeftToAllocate -= endByteIndex - startByteIndex + 1;
        }


        return ranges;
    }
}
