package com.company.downloader;

import com.google.inject.Inject;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

public class DefaultHttpClient implements CustomHttpClient {
    @Inject
    private CloseableHttpClient httpClient;

    @Override
    public HttpResponse execute(HttpGet request) throws IOException {
        return this.httpClient.execute(request);
    }

    @Override
    public void close() throws IOException {
        this.httpClient.close();
    }
}
