package com.company.downloader;

public interface RetryPolicy {
    RetryResult getRetry(int retryAttempts);
}
