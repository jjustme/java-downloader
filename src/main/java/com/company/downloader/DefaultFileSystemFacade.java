package com.company.downloader;

import java.io.*;

public class DefaultFileSystemFacade implements FileSystemFacade {
    public FileInputStream getFileInputStream(String path) throws FileNotFoundException {
        return new FileInputStream(path);
    }

    @Override
    public FileOutputStream getFileOutputStream(String path, boolean append) throws FileNotFoundException {
        return new FileOutputStream(path, append);
    }

    @Override
    public File createTempFile(String prefix, String suffix, String directory) throws IOException {
        return File.createTempFile(prefix, suffix, new File(directory));
    }

    @Override
    public File getFile(String path) {
        return new File(path);
    }
}
