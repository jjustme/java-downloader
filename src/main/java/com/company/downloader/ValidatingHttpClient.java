package com.company.downloader;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import java.io.Closeable;

public interface ValidatingHttpClient extends Closeable {
    HttpResponse execute(HttpGet request, long startByteIndex, long lastByteIndex) throws Exception;
}
