package com.company.downloader;

import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.List;

public interface RangeCalculator {
    List<ImmutablePair<Long, Long>> getRanges(long fileSize, int chunks);
}
