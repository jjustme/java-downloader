package com.company.di;

import com.company.downloader.*;
import com.company.downloader.utils.DefaultIOUtils;
import com.company.downloader.utils.IOUtils;
import com.google.inject.*;
import com.google.inject.name.Names;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

public class DownloaderModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Downloader.class).to(SegmentedDownloader.class);
        bind(SegmentDownloader.class).to(DefaultSegmentDownloader.class);
        bind(RangeCalculator.class).to(MicroSegmentsRangeCalculator.class);
        bind(int.class).annotatedWith(Names.named("MicroSegmentSize")).toInstance(8 * 1024 * 1024);
        bind(RetryPolicy.class).to(DefaultRetryPolicy.class);
        bind(IOUtils.class).to(DefaultIOUtils.class);
        bind(ValidatingHttpClient.class).to(DefaultValidatingHttpClient.class);
        bind(CustomHttpClient.class).to(DefaultHttpClient.class);
        bind(Scout.class).to(DefaultScout.class);
        bind(FileSystemFacade.class).to(DefaultFileSystemFacade.class);
        bind(SegmentDownloaderFactory.class).to(DefaultSegmentDownloaderFactory.class);
        bind(new TypeLiteral<ExecutorCompletionWrapper<SegmentDownloader, SegmentDownloadResult>>() {}).to(new TypeLiteral<DefaultExecutorCompletionWrapper<SegmentDownloader, SegmentDownloadResult>>() {});
    }

    @Provides
    @Inject
    public CloseableHttpClient providesCloseableHttpClient(HttpClientConnectionManager poolManager) {
        return this.buildHttpClient(poolManager);
    }

    private CloseableHttpClient buildHttpClient(HttpClientConnectionManager poolManager) {
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(10 * 1000)
                .setConnectTimeout(10 * 1000)
                .build();
        return HttpClients.custom().setDefaultRequestConfig(requestConfig).setConnectionManager(poolManager).setConnectionManagerShared(true).build();
    }

    @Provides
    @Singleton
    public HttpClientConnectionManager provideHttpClientConnectionManager() {
        PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
        poolingHttpClientConnectionManager.setMaxTotal(Integer.MAX_VALUE);
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(Integer.MAX_VALUE);
        return poolingHttpClientConnectionManager;
    }
}
