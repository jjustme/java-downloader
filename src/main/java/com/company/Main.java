package com.company;

import com.company.deserializer.HeaderDeserializer;
import com.company.deserializer.HeaderSerializer;
import com.company.di.DownloaderModule;
import com.company.downloader.DownloadConfiguration;
import com.company.downloader.SegmentedDownloader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.http.Header;

import java.io.File;

public class Main {

    public static void main(String[] args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(Header.class, new HeaderDeserializer());
        module.addSerializer(Header.class, new HeaderSerializer());
        mapper.registerModule(module);
        
        DownloadConfiguration configuration = mapper.readValue(new File(args[0]), DownloadConfiguration.class);

        Injector injector = Guice.createInjector(new DownloaderModule());
        SegmentedDownloader downloader = injector.getInstance(SegmentedDownloader.class);
        downloader.initialize(configuration);
        downloader.download();
    }
}
