package com.company.deserializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.apache.http.Header;

import java.io.IOException;

public class HeaderSerializer extends com.fasterxml.jackson.databind.JsonSerializer<Header> {
    @Override
    public void serialize(Header header, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", header.getName());
        jsonGenerator.writeStringField("value", header.getValue());
        jsonGenerator.writeEndObject();
    }
}
