package com.company.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.http.Header;
import org.apache.http.message.BasicHeader;

import java.io.IOException;

public class HeaderDeserializer extends com.fasterxml.jackson.databind.JsonDeserializer<Header> {
    @Override
    public Header deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String name = node.get("name").textValue();
        String value = node.get("value").textValue();

        return new BasicHeader(name, value);
    }
}
