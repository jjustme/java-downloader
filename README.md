# Java Downloader
Java Downloader is a file downloader written in Java.

## Features

* Download using multiple simultaneous connections
* Resume unfinished downloads
* Reuse connections
* Download from multiple mirrors

## Motivations

I needed to download large files (20GB+) from distant servers. Due to the distance (I think) when an off the shelf downloader would establish multiple connections to the server(s), some connections would have a download rate limit, seemingly randomly. This meant downloads would take much longer than necessary.

One downloader I used split up the file into equal segments assigning each segment to a connection. In this situation, if one of the established connections happened to be rate limited, the download would take the time it takes for the slowest connection to download it's segment.

Another downloader I used would split the remaining segments when one segment completes downloading however it would first close the connection and open a new one to download the new segment. When it establishes a new connection, it has a (seemingly) random chance of being rate limited.

To explain the last paragraph a little better, I'll use an example:

We want to download a 100B file (bytes indexed from 0 to 99) using 2 parallel connections. The file is split into two segments, with the following byte ranges: 0-49, 50-99. Both segments begin downloading in parallel using the 2 connections, the connection downloading the first segment is rate limited and has only downloaded 10 bytes by the time the other connection has finished downloading the second segment.

At this point the downloader splits the remaining bytes of the first segment so we have two segments again, this time with the following byte ranges: 10-29, 30-49, the slow connection can continue to download from it's current position but the fast connection gets dropped and a new one is established to download the byte range 30-49. If we're unlucky that new connection will also be rate limited and we'll end up with a slow download.

This downloader however does not have this issue because it reuses the connection after a segment finishes downloading so we hold onto our fast connections resulting in a faster download.

## Usage
First you need to create a download configuration file. This is a JSON file that looks something like this:
```
{
  "mirrors": [
    {
      "link": "http://ipv4.download.thinkbroadband.com/100MB.zip"
    },
    {
      "link": "http://ipv4.download.thinkbroadband.com:81/100MB.zip"
    }
  ],
  "maximumSimultaneousConnections": 4,
  "temporaryDirectory": "./temp",
  "outputPath": "./100MB.zip"
}
```
The link field on the mirror objects should point to the file you want to download, if there are multiple locations for the file, you can list multiple mirrors.

The temporary directory is a place where the downloader will download segments to before it joins them all together at the end into the output file. The temporary directory needs to be cleaned manually for now.

Then simply pass the file as an argument to the downloader:
```
java -jar downloader.jar config.json
```